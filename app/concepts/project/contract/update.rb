module Project::Contract
    class Update < Reform::Form
      include Dry
      property :title
      property :description
    end
  end
  