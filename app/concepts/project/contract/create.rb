module Project::Contract
  class Create < Reform::Form
    include Dry
    property :title
    property :description
  end
end
